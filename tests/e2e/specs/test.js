// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('http://localhost:8081/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
    cy.get('[data-cy=links]')
    .children()
    .should('have.length', 4)
    cy.get('[data-cy=eco]')
    .children()
    .should('have.length', 5)
  })
  it('Visits Grid', () => {
    cy.visit('http://localhost:8081/grid')
    cy.get('[id=agGrid]')
  })
  it('Visits Registration Form', () => {
    cy.visit('http://localhost:8081/FormValidation')
    cy.get('[id=registrationFrom]')
    cy.get('[type=submit]').click().contains('Submit Fail')
    cy.get('[data-cy=firstname]').type('Siva').wait(500)
      .get('[data-cy=lastname]').type('Jonnala').wait(500)
      .get('[data-cy=age]').type(25).wait(500)
      .get('[data-cy=username]').type('sivajonnala').wait(1000)
      .get('[data-cy=email]').type('sivajonnala33@gmail.com').wait(500)
      .get('[data-cy=password]').type('1234567890').wait(500)
      .get('[data-cy=repeatpassword]').type('1234567890').wait(500)
      .get('[data-cy=mobile]').type('989809898991').wait(500)
      .get('[data-cy=website]').type('https://sivajonnala1.com').wait(500)
      .get('[type=submit]').click().wait(500).contains('Submit Success')
  })
})
